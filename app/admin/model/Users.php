<?php
/***********************************************************
 * 会员中心模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Request;
class Users extends Common
{
    //列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('users')->field($tableWhere['field'])->where($tableWhere['where'])->order($tableWhere['order'])->paginate($limit);
        $data = $this->tableFormat($list->getCollection())->toArray(); 
        foreach ($data as $key=>$val){
            $test = Db::name("users_level")->where([['id','=',$val['level_id']]])->field('level_name')->find();
            $data[$key]['level_name'] =  $test['level_name'];
            $data[$key]['add_time'] = pcftime($val['add_time']);
            $data[$key]['login_time'] = pcftime($val['login_time']);
        }
        $re['code'] = 0;
        $re['msg'] = '';
        $re['count'] = $list->total();
        $re['data'] = $data;
        return $re;
    }

    //添加-编辑
    public function toAdd($data)
    {
        $result = array('code' => 0,'data' => '','msg' => '');
        //判断是新增还是修改
        if (isset($data['id']) && !empty($data['id'])) {
            $edit_data = array();
            if (!$data['username']){
                $result = ['code' => 0,'msg'=> '请填写用户名'];
                return $result;
            }
            if(empty($data['mobile']) || !newisMobile($data['mobile'])){
                $result = ['code' => 0,'msg'=> '输入正确手机'];
                return $result;
            }

            $users_reg_notallow = !empty(getUsersConfigData('users.users_reg_notallow')) ? getUsersConfigData('users.users_reg_notallow') : '';
            $users_reg1 = explode(",",$users_reg_notallow);
            if(in_array($data['username'],$users_reg1)){
                $result = ['code' => 0,'msg'=> '非法用户名！'];
                return $result;
            }
            $edit_data['level_id'] = $data['level_id'];
            $edit_data['email'] = $data['email']; 
            $edit_data['username'] = $data['username'];
            $edit_data['nickname'] = $data['nickname'] ? $data['nickname']:'游客';
            $edit_data['truename'] = $data['truename'];
            $edit_data['id'] = $data['id'];
            $edit_data['mobile'] = $data['mobile'];
            $edit_data['qq']  = $data['qq'];
            $edit_data['remark']  = $data['remark'];
            $edit_data['update_time'] = time();
            if($data['password']){
               $edit_data['password'] = func_encrypt($data['password']);
            }
            if (Db::name('users')->save($edit_data)) {
                $result = ['code' => 1,'msg'=> '修改成功','url'=>Request::baseFile().'/users.index/index'];
                return $result;

            } else {
                $result = ['code' => 0,'msg'=> '修改失败'];
                return $result;
            }
        } else {
            $add_data = array();
            if (!$data['username']){
                $result = ['code' => 0,'msg'=> '请填写用户名'];
                return $result;
            }
            if($data['mobile']){
                $infoemail = Db::name('users')->where('mobile', $data['mobile'])->find();
                if(!isEmail($data['mobile'])){
                    $result = ['code' => 0,'msg'=> '输入正确手机号码'];
                    return $result;
                }
                if ($infoemail['mobile'] == $data['mobile']){
                    $result = ['code' => 0,'msg'=> "操作失败，手机号码{$data['email']}已被注册"];
                    return $result;
                } 
            }
            if (!$data['password']){
                $result = ['code' => 0,'msg'=> '请填写密码'];
                return $result;
            }
            if (!$data['truename']){
                $result = ['code' => 0,'msg'=> '请填写真实姓名'];
                return $result;
            }
            $users_reg_notallow = !empty(getUsersConfigData('users.users_reg_notallow')) ? getUsersConfigData('users.users_reg_notallow') : '';
            $users_reg1 = explode(",",$users_reg_notallow);
            if(in_array($data['username'],$users_reg1)){
                $result = ['code' => 0,'msg'=> '非法用户名！'];
                return $result;
            }
            $add_data['mobile'] = $data['mobile'];
            $add_data['username'] = $data['username'];
            $add_data['nickname'] = $data['nickname'] ? $data['nickname']:'游客';
            $add_data['truename'] = $data['truename'];
            $add_data['qq']  = $data['qq'];
            $add_data['email'] = $data['email']; 
            $add_data['register_place'] = 1;
            $add_data['password'] = func_encrypt($data['password']);
            $add_data['level_id'] = $data['level_id'];
            $add_data['remark'] = $data['remark'];
            $add_data['add_time'] = time();
            $add_data['login_time'] = time();
            if (Db::name('users')->save($add_data)) {
                $result = ['code' => 1,'msg'=> '添加成功','url'=>Request::baseFile().'/users.index/index'];
                return $result;
            } else {
                $result = ['code' => 0,'msg'=> '添加失败'];
                return $result;
            }
        }
    }

    protected function pcftableWhere($post)
    {
        $where = [];
        if (isset($post['username']) && $post['username'] != "") {
            $where[] = ['username', 'like', '%' . $post['username'] . '%'];
        }
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "id desc";
        return $result;
    }
}
