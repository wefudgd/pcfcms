<?php
/***********************************************************
 * 字段模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;

use think\Model;
use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;

class FieldType extends Model
{

    //同步模型附加表的字段记录
    public function synChannelTableColumns($channel_id,$channel_name = '')
    {
        $where = [];
        if (!empty($channel_id)){
            $where[] = ['id','=',$channel_id];
        }
        if (!empty($channel_name)){
            $where[] = ['nid','=',strtolower($channel_name)];
        }
        if (empty($where)){
            return false;
        }
        $channeltypeInfo = Db::name('channel_type')->field('id,table,nid,ctl_name')->where($where)->select()->toArray();
        if (empty($channeltypeInfo)){
            return false;
        }
        $channelfieldArr = Db::name('channelfield')->where('channel_id',$channeltypeInfo['id'])->column('name,dtype', 'name');
        $this->synArchivesTableColumns($channeltypeInfo['id'],$channelfieldArr); //主表
        $this->synContentTableColumns($channeltypeInfo['id'],$channelfieldArr,$channeltypeInfo); //content表
        Cache::clear('channelfield');
    }
    //同步文档主表的字段记录到指定模型
    public function synArchivesTableColumns($channel_id = '',$channelfieldArr)
    {
        $new_arr = array(); // 表字段数组
        $addData = array(); // 数据存储变量
        $controlFields = ['litpic','author'];
        $channeltype_system_ids = Db::name('channel_type')->where('ifsystem', 1)->column('id');

        $table = config('database.connections.mysql.prefix').'archives';
        $row = Db::query("SHOW FULL COLUMNS FROM {$table}");
        $row = array_reverse($row);
        foreach ($row as $key => $val) {
            $fieldname = $val['Field'];
            $new_arr[] = $fieldname;
            // 对比字段记录 表字段有 字段新增记录没有
            if (empty($channelfieldArr[$fieldname])) {
                $dtype = $this->toDtype($val['Type']);
                $dfvalue = $this->toDefault($val['Type'], $val['Default']);
                if (in_array($fieldname, $controlFields) && !in_array($channel_id, $channeltype_system_ids)) {
                    $ifcontrol = 0;
                } else {
                    $ifcontrol = 1;
                }
                $maxlength = preg_replace('/^([^\(]+)\(([^\)]+)\)(.*)/i', '$2', $val['Type']);
                $maxlength = intval($maxlength);
                $addData[] = array(
                    'name'  => $fieldname,
                    'channel_id'  => $channel_id,
                    'title'  => !empty($val['Comment']) ? $val['Comment'] : $fieldname,
                    'dtype' => $dtype,
                    'define'    => $val['Type'],
                    'maxlength' => $maxlength,
                    'dfvalue'   => $dfvalue,
                    'ifeditable' => 1,
                    'ifsystem'  => 1,
                    'ifmain'    => 1,
                    'ifcontrol' => $ifcontrol,
                    'add_time'  => getTime(),
                    'update_time'  => getTime(),
                );
            }
        }
        if (!empty($addData)) {
            Db::name('channelfield')->insertAll($addData);
        }
        //字段新增记录有，表字段没有
        if($channelfieldArr){
            foreach($channelfieldArr as $k => $v){
                if (!in_array($k, $new_arr)) {
                    $map = array(
                        'channel_id'  => $channel_id,
                        'ifmain'    => 1,
                        'name'  => $v['name'],
                    );
                    Db::name('channelfield')->where($map)->delete();
                }
            }    
        }
    }
    //同步content表字段记录到指定模型
    private function synContentTableColumns($channel_id,$channelfieldArr,$channeltypeInfo){
        if (empty($channeltypeInfo['table'])){
            return false;
        }
        $tableExt = config('database.connections.mysql.prefix').$channeltypeInfo['table'].'_content';
        $rowExt = Db::query("SHOW FULL COLUMNS FROM {$tableExt}");
        $new_arr = array(); // 表字段数组
        $addData = array(); // 数据存储变量
        foreach ($rowExt as $key => $val) {
            $fieldname = $val['Field'];
            if (in_array($fieldname, array('id','add_time','update_time','aid','typeid'))) {
                continue;
            }
            $new_arr[] = $fieldname;
            // 对比字段记录 表字段有 字段新增记录没有
            if (empty($channelfieldArr[$fieldname])) {
                $dtype = $this->toDtype($val['Type']);
                $dfvalue = $this->toDefault($val['Type'], $val['Default']);
                $ifsystem = 1;
                $maxlength = preg_replace('/^([^\(]+)\(([^\)]+)\)(.*)/i', '$2', $val['Type']);
                $maxlength = intval($maxlength);
                $addData[] = array(
                    'name'  => $fieldname,
                    'channel_id'  => $channel_id,
                    'title'  => !empty($val['Comment']) ? $val['Comment'] : $fieldname,
                    'dtype' => $dtype,
                    'define'    => $val['Type'],
                    'maxlength' => $maxlength,
                    'dfvalue'   => $dfvalue,
                    'ifeditable'    => 1,
                    'ifsystem'  => $ifsystem,
                    'ifmain'    => 0,
                    'ifcontrol' => 0,
                    'add_time'  => getTime(),
                    'update_time'  => getTime(),
                );
            }
        }
        if (!empty($addData)) {
           Db::name('channelfield')->insertAll($addData);
        }
        $map = [];
        //字段新增记录有，表字段没有
        foreach($channelfieldArr as $k => $v){
            if (!in_array($k, $new_arr)) {
                $map[] = ['channel_id','=',$channel_id];
                $map[] = ['ifmain','=', 0];
                $map[] = ['name','=', $v['name']];
                Db::name('channelfield')->where($map)->delete();
            }
        }
    }
    //删除指定模型的表字段
    public function delChannelField($id)
    {
        $code = 0;
        $msg = '参数有误！';
        if (!empty($id)) {
            $id = intval($id);
            $row = Db::name('channelfield')->where('id',$id)->field('channel_id,name,ifsystem')->find();
            if (!empty($row['ifsystem'])) {
                return array('code'=>0, 'msg'=>'禁止删除系统字段！');
            }
            $fieldname = $row['name'];
            $channel_id = $row['channel_id'];
            $table = Db::name('channel_type')->where('id',$channel_id)->value('table');
            $table = config('database.connections.mysql.prefix').$table.'_content';
            if ($this->checkChannelFieldList($table, $fieldname, $channel_id)) {
                $sql = "ALTER TABLE `{$table}` DROP COLUMN `{$fieldname}`;";
                if(false !== Db::execute($sql)) {
                    return array('code'=>1, 'msg'=>'删除成功！');
                } else {
                    $code = 0;
                    $msg = '删除失败！'; 
                }
            } else {
                $code = 2;
                $msg = '字段不存在！';
            }
        }
        return array('code'=>$code, 'msg'=>$msg);
    }

    //表字段类型转为自定义字段类型
    public function toDtype($fieldtype = '')
    {
        if (preg_match('/^int/i', $fieldtype)) {
            $maxlen = preg_replace('/^int\((.*)\)/i', '$1', $fieldtype);
            if (10 == $maxlen) {
                $dtype = 'int';
            } else if (11 == $maxlen) {
                $dtype = 'datetime';
            }
        } else if (preg_match('/^longtext/i', $fieldtype)) {
            $dtype = 'htmltext';
        } else if (preg_match('/^text/i', $fieldtype)) {
            $dtype = 'multitext';
        } else if (preg_match('/^enum/i', $fieldtype)) {
            $dtype = 'select';
        } else if (preg_match('/^set/i', $fieldtype)) {
            $dtype = 'checkbox';
        } else if (preg_match('/^float/i', $fieldtype)) {
            $dtype = 'float';
        } else if (preg_match('/^decimal/i', $fieldtype)) {
            $dtype = 'decimal';
        } else if (preg_match('/^tinyint/i', $fieldtype)) {
            $dtype = 'switch';
        } else if (preg_match('/^varchar/i', $fieldtype)) {
            $maxlen = preg_replace('/^varchar\((.*)\)/i', '$1', $fieldtype);
            if (250 == $maxlen) {
                $dtype = 'img';
            } else if (1001 == $maxlen || 10001 == $maxlen) {
                $dtype = 'imgs';
            } else if (1002 == $maxlen || 10002 == $maxlen) {
                $dtype = 'files';
            } else {
                $dtype = 'text';
            }
        } else {
            $dtype = 'text';
        }

        return $dtype;
    }
    //表字段的默认值
    public function toDefault($fieldtype, $dfvalue = '')
    {
        if (preg_match('/^(enum|set)/i', $fieldtype)) {
            $str = preg_replace('/^(enum|set)\((.*)\)/i', '$2', $fieldtype);
            $str = str_replace("'", "", $str);
        } else {
            $str = $dfvalue;
        }
        $str = ("" != $str) ? $str : '';
        return $str;
    }
    /**
     * 检测频道模型相关的表字段是否已存在，包括：主表和附加表
     * @access    public
     * @param     string  $slave_table  附加表
     * @return    string $fieldname 字段名
     * @return    int $channel_id 模型ID
     * @param     array  $filter  过滤哪些字段
     */
    public function checkChannelFieldList($slave_table, $fieldname, $channel_id, $filter = array())
    {
        // 栏目表字段
        $arctypeFieldArr = Db::getTableFields(config('database.connections.mysql.prefix').'arctype'); 
        foreach ($arctypeFieldArr as $key => $val) {
            if (!preg_match('/^type/i',$val)) {
                array_push($arctypeFieldArr, 'type'.$val);
            }
        }
        $masterFieldArr = Db::getTableFields(config('database.connections.mysql.prefix').'archives'); // 文档主表字段
        $slaveFieldArr = Db::getTableFields($slave_table); // 文档附加表字段
        $addfields = ['pageurl','has_children','typelitpic','arcurl','typeurl']; // 额外与字段冲突的变量名
        $fieldArr = array_merge($slaveFieldArr, $masterFieldArr, $addfields, $arctypeFieldArr); // 合并字段
         if (!empty($fieldname)) {
            if (!empty($filter) && is_array($filter)) {
                foreach ($filter as $key => $val) {
                    $k = array_search($val, $fieldArr);
                    if (false !== $k) {
                        unset($fieldArr[$k]);
                    }
                }
            }
            return in_array($fieldname, $fieldArr);
        }
        return true;
    }
    /**
     * 检测指定表的字段是否已存在
     * @access    public
     * @param     string  $table  数据表
     * @return    string $fieldname 字段名
     * @param     array  $filter  过滤哪些字段
     */
    public function checkTableFieldList($table, $fieldname, $filter = array())
    {
        $fieldArr = Db::getTableFields($table); // 表字段
        if (!empty($fieldname)) {
            if (!empty($filter) && is_array($filter)) {
                foreach ($filter as $key => $val) {
                    $k = array_search($val, $fieldArr);
                    if (false !== $k) {
                        unset($fieldArr[$k]);
                    }
                }
            }
            return in_array($fieldname, $fieldArr);
        }
        return true;
    }
    /**
     * 获得字段创建信息
     * @access    public
     * @param     string  $dtype  字段类型
     * @param     string  $fieldname  字段名称
     * @param     string  $dfvalue  默认值
     * @param     string  $fieldtitle  字段标题
     * @return    array
     */
    function GetFieldMake($dtype, $fieldname, $dfvalue, $fieldtitle)
    {
        $fields = array();
        if("int" == $dtype)
        {
            $default_sql = '';
            if(preg_match("/^\d+$/",$dfvalue) )
            {
                $default_sql = "DEFAULT '$dfvalue'";
            }else{
                $default_sql = "DEFAULT '0'";
                $dfvalue = 0;
            }
            $maxlen = 10;
            $fields[0] = " `$fieldname` int($maxlen) NOT NULL $default_sql COMMENT '$fieldtitle';";
            $fields[1] = "int($maxlen)";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("datetime" == $dtype)
        {
            $default_sql = '';
            if(preg_match("#[0-9\-]#", $dfvalue))
            {
                $dfvalue = strtotime($dfvalue);
                empty($dfvalue) && $dfvalue = 0;
                $default_sql = "DEFAULT '$dfvalue'";
            }else{
                $dfvalue = "0";
            }
            $maxlen = 11;
            $fields[0] = " `$fieldname` int($maxlen) NOT NULL $default_sql COMMENT '$fieldtitle';";
            $fields[1] = "int($maxlen)";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("switch" == $dtype)
        {
            if(empty($dfvalue) || preg_match("#[^0-9]#", $dfvalue))
            {
                $dfvalue = 1;
            }else{
                $dfvalue = 0;
            }
            $maxlen = 1;
            $fields[0] = " `$fieldname` tinyint($maxlen) NOT NULL DEFAULT '$dfvalue' COMMENT '$fieldtitle';";
            $fields[1] = "tinyint($maxlen)";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("float" == $dtype)
        {
            $default_sql = '';
            if(preg_match("/^\d+\.*\d*$/", $dfvalue))
            {
                $default_sql = "DEFAULT '$dfvalue'";
            }else{
                $default_sql = "DEFAULT '0.00'";
                $dfvalue = "0.00";
            }
            $maxlen = 9;
            $fields[0] = " `$fieldname` float($maxlen,2) NOT NULL $default_sql COMMENT '$fieldtitle';";
            $fields[1] = "float($maxlen,2)";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("decimal" == $dtype)
        {
            $default_sql = '';
            if(preg_match("/^\d+\.*\d*$/", $dfvalue))
            {
                $default_sql = "DEFAULT '$dfvalue'";
            }else{
                $default_sql = "DEFAULT '0.00'";
                $dfvalue = "0.00";
            }
            $maxlen = 10;
            $fields[0] = " `$fieldname` decimal($maxlen,2) NOT NULL $default_sql COMMENT '$fieldtitle';";
            $fields[1] = "decimal($maxlen,2)";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("img" == $dtype)
        {
            if(empty($dfvalue)) {
                $dfvalue = '';
            }
            $maxlen = 250;
            $fields[0] = " `$fieldname` varchar($maxlen) NOT NULL DEFAULT '$dfvalue' COMMENT '$fieldtitle';";
            $fields[1] = "varchar($maxlen)";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("imgs" == $dtype)
        {
            if(empty($dfvalue)) {
                $dfvalue = '';
            }
            $maxlen = 10001;
            $fields[0] = " `$fieldname` varchar($maxlen) NOT NULL DEFAULT '$dfvalue' COMMENT '$fieldtitle';";
            $fields[1] = "varchar($maxlen)";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("files" == $dtype)
        {
            if(empty($dfvalue)) {
                $dfvalue = '';
            }
            $maxlen = 10002;
            $fields[0] = " `$fieldname` varchar($maxlen) NOT NULL DEFAULT '$dfvalue' COMMENT '$fieldtitle';";
            $fields[1] = "varchar($maxlen)";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("multitext" == $dtype)
        {
            $maxlen = 0;
            $fields[0] = " `$fieldname` text NOT NULL COMMENT '$fieldtitle';";
            $fields[1] = "text";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("htmltext" == $dtype)
        {
            $maxlen = 0;
            $fields[0] = " `$fieldname` longtext NOT NULL COMMENT '$fieldtitle';";
            $fields[1] = "longtext";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        else if("checkbox" == $dtype)
        {
            $maxlen = 0;
            $dfvalueArr = explode(',', $dfvalue);
            $default_value = isset($dfvalueArr[0]) ? $dfvalueArr[0] : '';
            $dfvalue = str_replace(',', "','", $dfvalue);
            $dfvalue = "'".$dfvalue."'";
            $fields[0] = " `$fieldname` SET($dfvalue) NOT NULL DEFAULT '{$default_value}' COMMENT '$fieldtitle';";
            $fields[1] = "SET($dfvalue)";
            $fields[2] = $maxlen;
            $fields[3] = $default_value;
        }
        else if("select" == $dtype || "radio" == $dtype)
        {
            $maxlen = 0;
            $dfvalueArr = explode(',', $dfvalue);
            $default_value = isset($dfvalueArr[0]) ? $dfvalueArr[0] : '';
            $dfvalue = str_replace(',', "','", $dfvalue);
            $dfvalue = "'".$dfvalue."'";
            $fields[0] = " `$fieldname` enum($dfvalue) NOT NULL DEFAULT '{$default_value}' COMMENT '$fieldtitle';";
            $fields[1] = "enum($dfvalue)";
            $fields[2] = $maxlen;
            $fields[3] = $default_value;
        }
        else
        {
            if(empty($dfvalue))
            {
                $dfvalue = '';
            }
            $maxlen = 200;
            $fields[0] = " `$fieldname` varchar($maxlen) NOT NULL DEFAULT '$dfvalue' COMMENT '$fieldtitle';";
            $fields[1] = "varchar($maxlen)";
            $fields[2] = $maxlen;
            $fields[3] = $dfvalue;
        }
        return $fields;
    }



    /**
     * 删除栏目的表字段
     * @param int $id channelfield表ID
     * @return bool
     */
    public function delArctypeField($id)
    {
        $code = 0;
        $msg = '参数有误！';
        if (!empty($id)) {
            $id = intval($id);
            $row = Db::name('channelfield')->where('id',$post['id'])->field('name,ifsystem')->find();
            if (!empty($row['ifsystem'])) {
                return array('code'=>0, 'msg'=>'禁止删除系统字段！');
            }
            $fieldname = $row['name'];
            $table = config('database.connections.mysql.prefix').'arctype';
            if ($this->checkTableFieldList($table, $fieldname)) {
                $sql = "ALTER TABLE `{$table}` DROP COLUMN `{$fieldname}`;";
                if(false !== Db::execute($sql)) {
                    //重新生成数据表字段缓存文件
                    return array('code'=>1, 'msg'=>'删除成功！');
                } else {
                    $code = 0;
                    $msg = '删除失败！'; 
                }
            } else {
                $code = 2;
                $msg = '字段不存在！';
            }
        }
        return array('code'=>$code, 'msg'=>$msg);
    }
    /**
     * 同步栏目主表的字段记录
     */
    public function synArctypeTableColumns($channel_id = '')
    {
        $cacheKey = "admin-FieldLogic-synArctypeTableColumns-{$channel_id}";
        $cacheValue = Cache::get($cacheKey);
        if (!empty($cacheValue)) {
            return true;
        }
        $global = get_global();
        $channel_id = !empty($channel_id) ? $channel_id : $global['arctype_channel_id'];
        $channelfieldArr = Db::name('channelfield')->where('channel_id',$channel_id)->column('name,dtype', 'name');
        $new_arr = array(); // 表字段数组
        $addData = array(); // 数据存储变量
        $table = config('database.connections.mysql.prefix').'arctype';
        $row = Db::query("SHOW FULL COLUMNS FROM {$table}");
        $row = array_reverse($row);
        $arctypeTableFields = $global['arctype_table_fields'];
        foreach ($row as $key => $val) {
            $fieldname = $val['Field'];
            $new_arr[] = $fieldname;
            // 对比字段记录 表字段有 字段新增记录没有
            if (empty($channelfieldArr[$fieldname])) {
                $dtype = $this->toDtype($val['Type']);
                $dfvalue = $this->toDefault($val['Type'], $val['Default']);
                if (in_array($fieldname, $arctypeTableFields)) {
                    $ifsystem = 1;
                } else {
                    $ifsystem = 0;
                }
                $maxlength = preg_replace('/^([^\(]+)\(([^\)]+)\)(.*)/i', '$2', $val['Type']);
                $maxlength = intval($maxlength);
                $addData[] = array(
                    'name'  => $fieldname,
                    'channel_id'  => $channel_id,
                    'title'  => !empty($val['Comment']) ? $val['Comment'] : $fieldname,
                    'dtype' => $dtype,
                    'define'    => $val['Type'],
                    'maxlength' => $maxlength,
                    'dfvalue'   => $dfvalue,
                    'ifeditable'    => 1,
                    'ifsystem' => $ifsystem,
                    'ifmain'    => 1,
                    'ifcontrol' => 1,
                    'add_time'  => getTime(),
                    'update_time'  => getTime(),
                );
            }
        }
        if (!empty($addData)) {
            Db::name('channelfield')->insertAll($addData);
        }
        $map=[];
        //字段新增记录有，表字段没有
        foreach($channelfieldArr as $k => $v){
            if (!in_array($k, $new_arr)) {
                $map[]=['channel_id' ,'=', $channel_id];
                $map[]=['name' ,'=', $v['name']];
                Db::name('channelfield')->where($map)->delete();
            }
        }
        Cache::clear('channelfield');
        Cache::clear("arctype");
        Cache::set($cacheKey, 1,PCFCMS_CACHE_TIME);
    }
    /**
     * 处理栏目自定义字段的值
     */
    public function handleAddonField($channel_id, $dataExt)
    {
        $nowDataExt = array();
        if (!empty($dataExt) && !empty($channel_id)) {
            $fieldTypeList = Db::name('channelfield')->where('channel_id',$channel_id)->column('name,dtype', 'name');
            foreach ($dataExt as $key => $val) {
                $key = preg_replace('/^(.*)(_gzpcf_is_remote|_gzpcf_remote|_gzpcf_local)$/', '$1', $key);
                $dtype = !empty($fieldTypeList[$key]) ? $fieldTypeList[$key]['dtype'] : '';
                switch ($dtype) {
                    case 'checkbox':
                    {
                        $val = implode(',', $val);
                        break;
                    }
                    case 'switch':
                    case 'int':
                    {
                        $val = intval($val);
                        break;
                    }
                    case 'img':
                    {
                        $is_remote = !empty($dataExt[$key.'_gzpcf_is_remote']) ? $dataExt[$key.'_gzpcf_is_remote'] : 0;
                        if (1 == $is_remote) {
                            $val = $dataExt[$key.'_gzpcf_remote'];
                        } else {
                            $val = $dataExt[$key.'_gzpcf_local'];
                        }
                        break;
                    }
                    case 'imgs':
                    case 'files':
                    {
                        foreach ($val as $k2 => $v2) {
                            if (empty($v2)) {
                                unset($val[$k2]);
                                continue;
                            }
                            $val[$k2] = trim($v2);
                        }
                        $val = implode(',', $val);
                        break;
                    }
                    case 'datetime':
                    {
                        $val = !empty($val) ? strtotime($val) : getTime();
                        break;
                    }
                    case 'decimal':
                    {
                        $moneyArr = explode('.', $val);
                        $money1 = !empty($moneyArr[0]) ? intval($moneyArr[0]) : '0';
                        $money2 = !empty($moneyArr[1]) ? intval(msubstr($moneyArr[1], 0, 2)) : '00';
                        $val = $money1.'.'.$money2;
                        break;
                    }
                    default:
                    {
                        $val = trim($val);
                        break;
                    }
                }
                $nowDataExt[$key] = $val;
            }
        }
        return $nowDataExt;
    }
    /*
     * 更新 pcf_channelfield 表 dfvalue_unit、remark字段
     */
    public function synChannelUnit(){
        Db::name('channelfield')->where("name='average_price' and ifsystem=1 and channel_id>10")->save(['dfvalue_unit'=>'元/㎡']);
        Db::name('channelfield')->where("name='building_age' and ifsystem=1 and channel_id>10")->save(['dfvalue_unit'=>'年']);
        Db::name('channelfield')->where("name='area' and ifsystem=1 and channel_id>10")->save(['dfvalue_unit'=>'㎡']);
        Db::name('channelfield')->where("name='building_area' and ifsystem=1 and channel_id>10")->save(['dfvalue_unit'=>'㎡']);
        Db::name('channelfield')->where("name='floor_area' and ifsystem=1 and channel_id>10")->save(['dfvalue_unit'=>'㎡']);
        Db::name('channelfield')->where("name='floor_count' and ifsystem=1 and channel_id>10")->save(['dfvalue_unit'=>'层']);
        Db::name('channelfield')->where("name='floor_count' and ifsystem=1 and channel_id>10")->save(['dfvalue_unit'=>'层']);
        Db::name('channelfield')->where("name='property' and ifsystem=1 and channel_id>10")->save(['dfvalue_unit'=>'年']);
        Db::name('channelfield')->where("name='total_price' and ifsystem=1 and channel_id>10")->save(['dfvalue_unit'=>'万元']);
    }

}
