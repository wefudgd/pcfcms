<?php
/***********************************************************
 * 自定义模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;

use think\facade\Db;
use think\facade\Session;
use think\facade\Cache;
use think\facade\Request;
use app\admin\logic\FieldLogic;

class Custom 
{
    protected function initialize()
    {
        parent::initialize();
    }

    // 列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 15;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list1 = DB::name('archives')->field("aid")->where($tableWhere['where'])->order($tableWhere['order'])->paginate(10,false,['channel' => $post['channel'],'typeid'=>$post['typeid']]);
        $data = $list1->getCollection()->toArray();
        $list = array_combine(array_column($data, 'aid'), $data);
        if ($list) {
            $aids = array_keys($list);
            $fields = "b.*, a.*, a.aid as aid";
            $row = DB::name('archives')
                ->field($fields)
                ->alias('a')
                ->join('arctype b', 'a.typeid = b.id', 'LEFT')
                ->where('a.aid', 'IN', $aids)
                ->column($fields, 'aid');
            foreach ($list as $key => $val) {
                $row[$val['aid']]['arcurl'] = get_arcurl($row[$val['aid']]);
                $row[$val['aid']]['update_time'] = date('Y-m-d', $row[$val['aid']]['update_time']);
                $row[$val['aid']]['litpic'] = handle_subdir_pic($row[$val['aid']]['litpic']);
                $list[$key] = $row[$key];
            }
            $list = array_merge($list);
        }
        $re['code'] = 0;
        $re['msg'] = 'ok';
        $re['count'] = $list1->total();
        $re['data'] = $list;
        return $re;
    }

    protected function pcftableWhere($post)
    {
        $this->channeltype = $post['channel'];
        $channeltypeRow = Db::name('channel_type')->field('nid')->where('id',$this->channeltype)->find();
        $nid = $channeltypeRow['nid'];
        $gzpcfglobal = get_global();
        $channeltype_list = $gzpcfglobal['channeltype_list'];
        $channeltype = $channeltype_list[$nid];
        $where = [];
        // 应用搜索条件
        foreach (['keywords','typeid'] as $key) {
            if (isset($post[$key]) && $post[$key] !== '') {
                if ($key == 'keywords') {
                    $where[] = ['title','LIKE',"%{$post[$key]}%"];
                } else if ($key == 'typeid') {
                    $typeid = $post[$key];
                    $arctype = new \app\common\model\Arctype;
                    $hasRow = $arctype->getHasChildren($typeid);
                    $typeids = get_arr_column($hasRow, 'id');
                    $where[] = ['typeid',"IN",$typeids];
                }else {
                    $where[] = [$key,'=',$post[$key]];
                }
            }
        }
        // 模型ID
        $where[] = ['channel','=',$channeltype];
        $where[]= ['is_del','=',0];
        $admin_info = session::get('admin_info');
        if (0 < intval($admin_info['role_id'])) {
            $auth_role_info = $admin_info['auth_role_info'];
            if(!empty($auth_role_info)){
                if(isset($auth_role_info['only_oneself']) && (1 == $auth_role_info['only_oneself'])){
                    $where[]= ['admin_id','=',$admin_info['admin_id']];
                }
            }
        }
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "sort_order desc,aid desc";
        return $result;
    }


    // 后置操作方法
    public function afterSave($aid, $post, $opt)
    {
        $fieldLogic = new FieldLogic();
        $post['aid'] = $aid;
        $addonFieldExt = !empty($post['addonFieldExt']) ? $post['addonFieldExt'] : array();
        $fieldLogic->dealChannelPostData($post['channel'], $post, $addonFieldExt);
    }

    // 获取单条记录
    public function getInfo($aid, $field = null, $isshowbody = true)
    {
        $result = array();
        $field = !empty($field) ? $field : '*';
        $result = Db::name('archives')->field($field)->where('aid', $aid)->find();
        if ($isshowbody) {
            $tableName = Db::name('channel_type')->where('id',$result['channel'])->value('table');
            $result['addonFieldExt'] = Db::name($tableName.'_content')->where('aid',$aid)->find();
        }
        return $result;
    }

    // 删除的后置操作方法
    public function afterDel($aidArr = array(), $table)
    {
        if (is_string($aidArr)) {
            $aidArr = explode(',', $aidArr);
        }
        // 同时删除内容
        Db::name($table.'_content')->where('aid','IN', $aidArr)->delete();
    }
}