<?php
/***********************************************************
 * 友情链接模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Request;
class Links extends Common
{

    //列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('links')->field($tableWhere['field'])->where($tableWhere['where'])->order($tableWhere['order'])->paginate($limit);
        $data = $this->tableFormat($list->getCollection())->toArray();
        foreach ($data as $key => $value) {
            $data[$key]['add_time'] = pcftime($value['add_time']);
        }
        $re['code'] = 0;
        $re['msg'] = '';
        $re['count'] = $list->total();
        $re['data'] = $data;
        return $re;
    }

    //添加/编辑
    public function toAdd($data)
    {
        $result = array('status' => false,'data' => '','msg' => '','url' => '');
        $domain = Request::baseFile().'/channel.Links/index';
        //判断是新增还是修改
        if (isset($data['id']) && !empty($data['id'])) {
            $edit_data = array();
            $info = Db::name('links')->where('id', $data['id'])->find();
            if (!$info) {
                $result['status'] = false;
                $result['msg'] = "广告不存在";
                return $result;
            }
            $edit_data['typeid'] = $data['typeid'];
            $edit_data['url'] = $data['url'];
            $edit_data['title'] = $data['title'];
            $edit_data['logo'] = $data['logo'];
            $edit_data['sort_order'] = $data['sort_order'];
            $edit_data['email'] = $data['email'];
            $edit_data['intro'] = $data['intro'];
            $edit_data['target'] = $data['target'];
            $edit_data['update_time'] = time();
            if (Db::name('links')->where('id', $data['id'])->data($edit_data)->update()) {
                $result['msg']    = '修改成功';
                $result['status'] = true;
                $result['url'] = $domain;
                return $result;
            } else {
                $result['msg']    = '修改失败';
                $result['status'] = false;
                return $result;
            }
        } else {
            $add_data = array();
            //判断用户名是否重复
            $info = Db::name('links')->where('title',$data['title'])->find();
            if ($data['title'] == $info['title']){
                $result['status'] = false;
                $result['msg'] = "标题已存在";
                return $result;
            }
            $add_data['typeid'] = $data['typeid'];
            $add_data['url'] = $data['url'];
            $add_data['title'] = $data['title'];
            $add_data['logo'] = $data['logo'];
            $add_data['sort_order'] = $data['sort_order'];
            $add_data['email'] = $data['email'];
            $add_data['intro'] = $data['intro'];
            $add_data['target'] = $data['target'];
            $add_data['add_time'] = time();
            if (Db::name('links')->save($add_data)) {
                $result['msg']    = '添加成功';
                $result['status'] = true;
                $result['url'] = $domain;
                return $result;
            } else {
                $result['msg']    = '添加失败';
                $result['status'] = false;
                return $result;
            }
        }
    }

    protected function pcftableWhere($post)
    {
        $where = [];
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = [];
        return $result;
    }
}
