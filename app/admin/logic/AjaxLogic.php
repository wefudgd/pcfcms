<?php
/***********************************************************
 * 逻辑定义
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\logic;
use think\facade\Db;
use think\facade\Request;
class AjaxLogic 
{
    //进入登录页面需要异步处理的业务
    public function login_handle()
    {
        $this->saveBaseFile(); // 存储后台入口文件路径，比如：/login.php
        $this->clear_session_file(); // 清理过期的data/session文件
    }

    //进入欢迎页面需要异步处理的业务 小潘 by 2020.03.10
    public function welcome_handle()
    {
        $this->saveBaseFile(); // 存储后台入口文件路径，比如：/login.php
        $this->renameInstall(); // 重命名安装目录，提高网站安全性
        $this->del_adminlog(); // 只保留最近三个月的操作日志
    }

    //清理过期的data/session文件 小潘 by 2020.03.05
    private function clear_session_file()
    {
        $path = ROOT_PATH .'runtime/session'; 
        if (!empty($path) && file_exists($path)) {
            $files = glob($path.'/sess_*');
            foreach ($files as $key => $file) {
                $filemtime = filemtime($file);
                if (time() - intval($filemtime) > config('params.login_expire')) {
                    @unlink($file);
                }
            }
        }
    }

    /**
     * 存储后台入口文件路径，比如：/login.php
     * 在 login 和 index 操作下
     * 小潘 by 2020.03.05
    */
    private function saveBaseFile()
    {
        $baseFile = request::baseFile();
        tpCache('web', ['web_adminbasefile'=>$baseFile]);
    }

    //只保留最近三个月的操作日志 小潘 by 2020.03.10
    private function del_adminlog()
    {
        $mtime = strtotime("-1 month");
        Db::name('admin_log')->where('log_time','<', $mtime)->delete();
    }

    /**
     * 重命名安装目录，提高网站安全性
     * 在 Admin@login 和 Index@index 操作下
     * 小潘 by 2020.03.10
     */
    private function renameInstall()
    {
        $install_path = ROOT_PATH .'public/install';
        if (is_dir($install_path) && file_exists($install_path)) {
            $install_time = time();
            $new_path = ROOT_PATH .'public/install_'.$install_time;
            @rename($install_path, $new_path);
        }
    }

}
