<?php
/**
 * 公共频道模型
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\common\model;

use think\Model;
use think\facade\Db;
use think\facade\Cache;
class ChannelType extends Model
{
    //初始化
    protected function initialize()
    {
        parent::initialize();
    }

    // 获取单条记录 小潘 by 2020.03.23
    public function getInfo($id)
    {
        $result = Db::name('channel_type')->field('*')->where('id',$id)->cache(true,PCFCMS_CACHE_TIME,"channeltype")->find();
        return $result;
    }

    // 获取单条记录 小潘 by 2020.03.23
    public function getInfoByWhere($where, $field = '*')
    {
        $result = Db::name('channel_type')->field($field)->where($where)->find();
        return $result;
    }

    // 获取多条记录 小潘 by 2020.03.23
    public function getListByIds($ids, $field = '*')
    {
        $map=[];
        $map[]=['id','IN',$ids];
        $result = Db::name('channel_type')->field($field)
            ->where($map)
            ->order('sort_order asc')
            ->select()->toArray();
        return $result;
    }

    // 默认获取全部 小潘 by 2020.03.23
    public function getAll($field = '*', $map = array(), $index_key = '')
    {
        $cacheKey = array('common','model','Channeltype','getAll',$field,$map,$index_key);
        $cacheKey = json_encode($cacheKey);
        $result = cache::get($cacheKey);
        if (empty($result)) {
            $result = Db::name('channel_type')->field($field)
                ->where($map)
                ->order('sort_order asc, id asc')
                ->select()->toArray();
            if (!empty($index_key)) {
                $result = convert_arr_key($result, $index_key);
            }
            Cache::tag('channeltype')->set($cacheKey, $result);
        }
        return $result;
    }

    // 根据文档ID获取模型信息 小潘 by 2020.03.23
    public function getInfoByAid($aid)
    {
        $result = array();
        $res1 = Db::name('archives')->where('aid',$aid)->find();
        $res2 = Db::name('channel_type')->where('id',$res1['channel'])->find();
        if (is_array($res1) && is_array($res2)) {
            $result = array_merge($res1, $res2);
        }
        return $result;
    }

    // 根据前端模板自动开启系统模型 小潘 by 2020.03.23
    public function setChanneltypeStatus()
    {
        $planPath = 'template/pc';
        $planPath = realpath($planPath);
        if (!file_exists($planPath)) {
            return true;
        }
        $ctl_name_arr = array();
        $dirRes   = opendir($planPath);
        $view_suffix = config('view.view_suffix');
        while($filename = readdir($dirRes))
        {
            if(preg_match('/^(lists|view)?_/i', $filename) == 1)
            {
                $tplname = preg_replace('/([^_]+)?_([^\.]+)\.'.$view_suffix.'$/i', '${2}', $filename);
                $ctl_name_arr[] = ucwords($tplname);
            } elseif (preg_match('/\.'.$view_suffix.'$/i', $filename) == 1) {
                $tplname = preg_replace('/\.'.$view_suffix.'$/i', '', $filename);
                $ctl_name_arr[] = ucwords($tplname);
            }
        }
        $ctl_name_arr = array_unique($ctl_name_arr);
        if (!empty($ctl_name_arr)) {
            Db::name('channel_type')->where('id > 0')->cache(true,null,"channeltype")->update(array('status'=>0, 'update_time'=>getTime()));
            $map = array('ctl_name' => array('IN', $ctl_name_arr),);
            Db::name('channel_type')->where($map)->cache(true,null,"channeltype")->update(array('status'=>1, 'update_time'=>getTime()));
        } 
    }

    /**
     * 获取有栏目的模型列表
     * @param string $type yes表示存在栏目的模型列表，no表示不存在栏目的模型列表
     *  小潘 by 2020.03.23
     */
    public function getArctypeChannel($type = 'yes')
    {
        if ($type == 'yes') {
            $map = [];
            $map[] = array('b.status','=',1);
            $map[] = array('b.is_del','=',0);
            $result = Db::name('channel_type')->field('b.*, a.*, b.id as typeid')
                ->alias('a')
                ->join('arctype b', 'b.current_channel = a.id', 'LEFT')
                ->where($map)
                ->group('a.id')
                ->order('a.sort_order asc, a.id asc')
                ->select()->toArray();
            $newresult  = array();
            foreach ($result as $key => $value) {
                $newresult[$value['nid']]= $value;
            }
        } else {
            $result = Db::name('channel_type')->field('b.*, a.*, b.id as typeid')
                ->alias('a')
                ->join('arctype b', 'b.current_channel = a.id', 'LEFT')
                ->group('a.id')
                ->order('a.sort_order asc, a.id asc')
                ->select()->toArray();
            $newresult  = array();
            foreach ($result as $key => $value) {
                $newresult[$value['nid']]= $value;
            }
            if ($newresult) {
                foreach ($newresult as $key => $val) {
                    if (intval($val['channeltype']) > 0) {
                        unset($newresult[$key]);
                    }
                }
            }
        }
        return $newresult;
    }
}